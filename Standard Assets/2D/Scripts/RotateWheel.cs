﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWheel : MonoBehaviour
{

    private Rigidbody2D r2d;
    public float speed = 5000.0f;

    void Start()
    {
        r2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        r2d.MoveRotation(r2d.rotation + speed * Time.fixedDeltaTime);
    }
}
