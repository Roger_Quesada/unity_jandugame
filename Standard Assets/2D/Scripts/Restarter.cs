﻿using UnityEngine;
using System.Collections;

namespace UnitySampleAssets._2D
{



    public class Restarter : MonoBehaviour
    {

        // Use this for initialization
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
                Application.LoadLevel(Application.loadedLevelName);
        }
    }
}
