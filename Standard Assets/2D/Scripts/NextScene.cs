﻿using UnityEngine;
using System.Collections;

public class NextScene : MonoBehaviour
{

    // Use this for initialization
    private bool door;

    void Awake()
    {
        door = true;
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (door)
        {
            Application.LoadLevel("credits");
        }
        door = false;
    }
    private void OnCollisionExit2D(Collision2D col)
    {
        if (!door)
        {
            door = true;
        }
    }
}

